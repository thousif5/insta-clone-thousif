import React, { Component } from 'react';
import data from './dummy-data';
import HeaderContainer from './components/HeaderContainer';
import PostContainer from './components/PostContainer';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div>
        <HeaderContainer />
        { data.map(item => 
          <PostContainer thumbnail = {item.thumbnailUrl} user = {item.username} image = {item.imageUrl} likesCount = {item.likes} comments = {item.comments} />
        )
        }
        </div>
      </div>
    );
  }
}

export default App;
