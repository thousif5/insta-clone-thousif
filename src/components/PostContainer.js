import React, { Component } from 'react';
import '../App.css';

export class PostContainer extends Component {
  render() {
    return (
      <div className = 'postDiv'>
        <div className='feedData'>
          <div className='dp'>
            <img src={this.props.thumbnail} alt=''></img>
            <h3>{this.props.user}</h3>
          </div>
          <div className='postImage'>
            <img src={this.props.image} alt=''></img>
          </div>
        </div>
        <div className='comment-section'>
          <div className='comment-icons'>
            <img id='like' src='https://files.slack.com/files-pri/T5XRADDQV-FHVC8PQGN/heart.png' alt=''></img>
            <img id='comment' src='https://buzzhostingservices.com/images/instagram-comment-icon-1.png' alt=''></img>
          </div>
          <div className='likesCount'>
            <p>{this.props.likesCount} likes</p>
          </div>
          {this.props.comments.map(comment =>
            <div className='comments'>
              <h4 style={{ margin: '0' }}>{comment.username}&nbsp;</h4>
              <p style={{ margin: '0' }}>{comment.text}</p>
            </div>
          )}
          <div className = 'addComment'>
            <input type = 'text' placeholder = 'Add Comment'></input>
          </div>
        </div>
      </div>
    )
  }
}

export default PostContainer
