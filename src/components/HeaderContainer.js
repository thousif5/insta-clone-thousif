import React, { Component } from 'react';
import './../App.css';

export class HeaderContainer extends Component {
  render() {
    return (
      <div className='header-title'>
        <div className = 'to-adjust'>
        <div className='logo'>
          <img className='logo-img' src='http://assets.sendible.com.s3.amazonaws.com/blog/images/17-dec/sm-icons-instagram-glyph-logo.png' alt=''></img>
        </div>
        <div className='divider'></div>
        <div className='logo-title'>
          <img className='logo-title-img' src='https://upload.wikimedia.org/wikipedia/commons/thumb/2/2a/Instagram_logo.svg/1200px-Instagram_logo.svg.png' alt=''></img>
        </div>
        </div>
        <div className = 'search'>
          <input type = 'text' placeholder = '&#xf002; search' />
        </div>
        <div className = 'header-icons'>
          <img src = 'https://files.slack.com/files-pri/T5XRADDQV-FHK6ZR517/discovery-icon.png' alt = ''></img>
          <img src = 'https://files.slack.com/files-pri/T5XRADDQV-FHVC8PQGN/heart.png' alt = ''></img>
          <img src = 'https://files.slack.com/files-pri/T5XRADDQV-FHE43JJ58/profile.png' alt = ''></img>
        </div>
      </div>
    )
  }
}

export default HeaderContainer
